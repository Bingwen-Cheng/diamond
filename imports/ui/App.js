import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Home from './Pages/Home/Home';
import MenuPage from './Pages/Menu/Menu';
import Cart from './Pages/Cart/Cart';
import Profile from './Pages/Profile/Profile';
import { Menu } from "antd";
import AccountsUIWrapper from './AccountsUIWrapper.js';


const AppNav = () => {
  return <div id='app-nav'>
    <Menu mode='horizontal'>
      <Menu.Item>
        <Link to="/">Home</Link>
      </Menu.Item>
      <Menu.Item>
        <Link to="/menu/">Menu</Link>
      </Menu.Item>
      <Menu.Item>
        <Link to="/cart/">Cart</Link>
      </Menu.Item>
      <Menu.Item>
        <Link to="/profile/">Profile</Link>
      </Menu.Item>
      <Menu.Item id='login'>
        <AccountsUIWrapper />
      </Menu.Item>
    </Menu>
  </div>
}

function AppRouter() {
    return (
      <div>
        <Route path="/" exact component={Home} />
        <Route path="/menu/" component={MenuPage} />
        <Route path="/cart/" component={Cart} />
        <Route path="/profile/" component={Profile} />
      </div>
    );
  }
  
  const App = () => (
    <Router>
      <div>
        <AppNav />
        <AppRouter />
      </div>
    </Router>
  )
  
  export default App;
  