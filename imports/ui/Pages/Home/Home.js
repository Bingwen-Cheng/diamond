import React from "react";
import { Menu, Card} from "antd";
import MenuItem from "antd/lib/menu/MenuItem";

const weeklySpecialties = [
  {
    title: 'Monday',
    dish: 'Beef',
    day: 1
  },
  {
    title: 'Tuesday',
    dish: 'Beef',
    day:2
  },
  {
    title: 'Wednesday',
    dish: 'Beef',
    day:3
  },
  {
    title: 'Thursday',
    dish: 'Beef',
    day:4
  },
  {
    title: 'Friday',
    dish: 'Beef',
    day:5
  },
  {
    title: 'Saturday',
    dish: 'Beef',
    day:6
  },
  {
    title: 'Sunday',
    dish: 'Beef',
    day:0
  }
]


  
const Home = () => {
  const mapSpecial = (special) => {
    const today=new Date()
    const cardId = today.getDay() == special.day ?
      'specialty-card-highlight':'specialty-card'
    return <div id='today-special'>
      <Card id={cardId}
        hoverable
        title={special.title}>
          <p>{special.dish}</p>
      </Card>
    </div>
  }

  return <div id='home'>
    <div id='side-nav'>
      <Menu mode="inline"
        // onClick={handleClick}
      >
        <MenuItem>
          <a href='#primary-events'>primary events</a>
        </MenuItem>
        <MenuItem>
          <a href='#weekly-specialties'>weekly-specialties</a>
        </MenuItem>
        <MenuItem>
          <a href='#secondary-events'>secondary events</a>
        </MenuItem>
        <MenuItem>
          <a href='#about-us'> About us</a>
         
        </MenuItem>
      </Menu>
    </div>
    <div id='content'>
      <div id='primary-events'>
        <div id="primary-event1">
          <Card >
            <h2> primary event1</h2>
          </Card>
        </div>
        <div id="primary-event2">
          <Card >
            <h2>primary event 2</h2>
          </Card>
        </div>
      </div>
      <div id='weekly-specialties'>
          {
            weeklySpecialties.map(mapSpecial)
          }
      </div>
      <div id='secondary-events'>
        <div id='secondary-event1'>
          <Card>
            <h2>secondary event 1</h2>
          </Card>
        </div>
        <div id='secondary-event2'>
          <div id='secondary-event2-1'>
            <Card>
            <h2>secondary event 2</h2>
            </Card>
          </div>  
          <div id='secondary-event2-2'>
            <Card>
              <h2>secondary event 3</h2>
            </Card>
          </div>
        </div>
      </div>
      <div >
        <h2>about us</h2>
        <div id='about-us'>
          <div id='map'>
            <Card>
              <h2>map</h2>
            </Card>
          </div>
          <div id='intro'>
            <h2>intro</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
}

export default Home