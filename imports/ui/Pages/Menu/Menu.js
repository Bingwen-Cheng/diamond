import React, { useState } from "react";
import { Menu, Button, Avatar, Card, Icon, Modal, Form, Input} from 'antd';
import { withTracker } from 'meteor/react-meteor-data';
import MenuItem from "antd/lib/menu/MenuItem";
import { compose, withStateHandlers, flattenProp } from "recompose";
import AddDish from './Components/AddDish'
import {Dishes} from '../../../api/models'
import { Meteor } from "meteor/meteor";
import fp from 'lodash/fp';
import { menuSideNavConfig } from "../../../Constants/menu";

const MenuPage = ( {dishes, setSelectedTags} ) => {

  const removeDish = (id) => {
    Meteor.call('dishes.remove', id)
  }

  const [openKeys, setOpenKeys] = useState([])

  const onOpenChange = (keys) => {
    setOpenKeys(fp.difference(keys)(openKeys))
  }

  const handleClickItem = (tags) => () => {
    setSelectedTags(tags)
  }
 

  return <div id='menu'>
    <div id='side-nav'>
      <Menu
        mode='inline'
        openKeys={openKeys}
        onOpenChange={onOpenChange}
      >
        {
          fp.map(
            ({name, tags, children}) => {
              const isSubMenu = !fp.isEmpty(children)
              return isSubMenu ?
                <Menu.SubMenu key={name} title={name}>
                  {
                    fp.map(
                      (child) => {
                        return <MenuItem onClick={handleClickItem(child.tags.concat(tags))} key={child.name}>
                          {child.name}
                        </MenuItem>
                      }
                    )(children)
                  }
                </Menu.SubMenu> :
                <MenuItem onClick={handleClickItem(tags)} key={name}>
                  {name}
                </MenuItem>
            }
          )(menuSideNavConfig.items)
        }
      </Menu>
    </div>
    <div id='content'>
      <div id='edit'><AddDish /></div>
        <div id='dish-list'>
          {dishes.map(dish=>{
            return <Card id='dish' key={dish._id}>
              <Card.Meta avatar={
                <Avatar src={fp.get('image.[0].thumbUrl')(dish)} />
              }/>
                <p>name: {dish.name}</p>
                <p>price: {dish.price}</p>
                <p>ingredients: {dish.tag.join(', ')}</p>
                <p>description: {dish.description}</p>
                <Button onClick={() => removeDish(dish._id)}>remove</Button>
              </Card>
            })
          }
        </div>
      </div>
    </div>
}
export default compose(
  withStateHandlers(
    {
      selectedTags: []
    },
    {
      setSelectedTags: () => (selectedTags) => ({
        selectedTags
      })
    }
  ),
  withTracker(
    ({selectedTags}) => {
      Meteor.subscribe('dishes');
      return ( { 
        dishes: Dishes.find({
          tag: {
            $all: selectedTags
          }
        }).fetch(),
      })
    }
  )
)(MenuPage)