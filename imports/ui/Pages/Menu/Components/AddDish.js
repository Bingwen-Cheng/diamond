import React from "react";
import { Button, Modal, Form, Input, InputNumber, Select, Upload, Icon } from 'antd';
import { withTracker } from 'meteor/react-meteor-data';
import { compose, withStateHandlers } from "recompose";
import { Meteor } from 'meteor/meteor'
import { tags } from "../../../../Constants/menu";


const AddDish = ( { form, showModal, modalVisible, handleOK, handleCancel } ) => {
    const {getFieldDecorator} = form
    const { Option } = Select;
    const handleSubmit = (e) => {
      e.preventDefault();
      form.validateFields((err, dish) => {
        if (!err) {
          console.log("submit",dish)
          Meteor.call('dishes.add', dish)
        }
      });
    };

    normFile = (e) => {
      console.log('Upload event:', e);
      if (Array.isArray(e)) {
        return e;
      }
      return e && e.fileList;
    };

    return <div id='add-button'>
    <Button onClick={showModal}>Add</Button>
    <Modal
      title="Add New Dish"
      visible={modalVisible}
      onOK={handleOK}
      onCancel={handleCancel}
      footer={[]}
    >
      <Form onSubmit={handleSubmit}>
        <Form.Item label="name">
          {
            getFieldDecorator('name',
              {
                rules:[{required:true, message:'Please input the dish name!'}]
              }
            )(<Input />)
          }
        </Form.Item>

        <Form.Item label="Price">
          {
            getFieldDecorator('price',
              { initialValue: 0 }
            )(<InputNumber min={1} max={88} />)
          }
        </Form.Item>

        <Form.Item label="Type" hasFeedback>
          {
            getFieldDecorator('type', {
              rules: [{ required: true, message: 'Please select the type!' }],
            }
            )(
              <Select placeholder="Please select a type">
                <Option value={tags.entree}>{tags.entree}</Option>
                <Option value={tags.main}>{tags.main}</Option>
              </Select>,
            )
          }
        </Form.Item>

        <Form.Item label="Upload Image" extra="notice the size">
          {
            getFieldDecorator('image',
              {
                valuePropName: 'fileList',
                getValueFromEvent: this.normFile,
              }
            )(
              <Upload name="logo" action="/upload.do" listType="picture">
                <Button>
                  <Icon type="upload" /> Click to upload
                </Button>
              </Upload>,
            )
          }
        </Form.Item>

        <Form.Item label="Ingredients">
          {
            getFieldDecorator('ingredients',
            {
              rules: [
                { 
                  required: true,
                  message: 'Please select your ingredients',
                  type: 'array' 
                },
              ],
            }
            )(
              <Select mode="multiple" placeholder="Please select ingredients">
                <Option value={tags.beef}>{tags.beef}</Option>
                <Option value={tags.chicken}>{tags.chicken}</Option>
                <Option value={tags.lamb}>{tags.lamb}</Option>
              </Select>,
            )
          }
        </Form.Item>

        <Form.Item label="Description">
          {
            getFieldDecorator('description',
              {
                rules: [{ required: true, message: 'Please describe the dish!' }],
              }
            )(<Input />)
          }
        </Form.Item>

        <Form.Item wrapperCol={{ span: 12, offset: 6 }}>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
        
      </Form>
    </Modal>
  </div>
}

export default compose(
    withStateHandlers(
      {
        modalVisible:false
      },
      {
        showModal: () => () => ({
          modalVisible: true
        }),
        handleOK: () => () => ({
          modalVisible: true
        }),
        handleCancel: () => () => ({
          modalVisible: false
        })
      }
    ),
    withTracker(
      () => {
        return ( { 
  
        })
      }
    )
  )(Form.create({ name: 'validate_other' })(AddDish))