import { Dishes } from './models';
import { Meteor } from 'meteor/meteor';

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish( 'dishes', function dishesPublication () {
    // const query = {
    //   type: keyword
    // }
    // const options = {
    //   sort: {
    //     createdAt: -1
    //   }
    // }
    return Dishes.find();
  } );

}

Meteor.methods({
  'dishes.add'({ name,image,price,type,ingredients,description }) {
    console.log('call add method')
    // Make sure the user is logged in before inserting a task
    const dish = Dishes.findOne({
      name: name
    })
    if(dish){
      throw new Meteor.Error('dish exist')
    }
    const newDish = {
      name: name,
      image: image,
      price: price,
      tag: [type].concat(ingredients) ,
      description: description,
      updateAt: new Date(),
      owner: this.userId,
    }
    Dishes.insert(newDish)
  },

  'dishes.remove'(id) {
    const dish = Dishes.findOne({
      _id: id 
    })
    if(dish){
      Dishes.remove(id)
    }else{
      throw new Meteor.Error('this dish not exist')
    }
  },





  });
  
  
//   'groups.updateDetails'(groupId, details) {
//     // Make sure the user is logged in before inserting a task
//     if (! this.userId) {
//       throw new Meteor.Error('not-authorized');
//     }

//     Groups.update( groupId, {
//       $set: {
//         details: details
//       }
//     })
//   }

