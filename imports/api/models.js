import { Mongo } from 'meteor/mongo';

export const Dishes = new Mongo.Collection('dishes');
export const CartItems = new Mongo.Collection('cartItems');
export const Orders = new Mongo.Collection('orders')