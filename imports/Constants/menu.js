export const tags = {
  chefSpecial: `CHEF'S SPECIAL`,
  entree: `ENTREE`,
  soup: `SOUP`,
  freshVege: `FRESH VEGETABLES`,
  main: `MAIN COURSE`,
  malay: `MALAYSIAN & THAI`,
  viet: `VIETNAMESE STYLE`,
  omelette: `OMELETTE`,
  noodles: `NOODLES`,
  chowMein: `CHOW MEIN`,
  rice: `RICE`,
  conventional: `CONVENTIONAL CHINESE CUISINE`,
  packs: `SPECIAL PACKS`,
  drinks: `DRINKS`,

  beef: `BEEF`,
  calamari: `CALAMARI`,
  fish: `FISH FILLET`,
  prawn: `KING PRAWN`,
  scallop: `SCALLOP`,
  chicken: `CHICKEN`,
  lamb: `LAMB`,
  duck: `DUCK`,
  pork: `PORK`,
  seafood: `SEAFOOD`,

  curry: `RED/GREEN/MASSAMAN CURRY`,
  thaiChilli: `THAI CHILLI`,
  malaySatay: `MALAY PEANUT SATAY`,
  laksa: `LAKSA`,
  malayChilli: `MALAY HOT CHILLI`,
  padThai: `PAD THAI NOODLES`
}

const getMenuItem = (name, tags, children) => ({
  name,
  tags,
  children
})

export const menuSideNavConfig = {
  items: [
    getMenuItem(tags.chefSpecial, [tags.chefSpecial], []),
    getMenuItem(tags.entree, [tags.entree], []),
    getMenuItem(tags.soup, [tags.soup], []),
    getMenuItem(tags.freshVege, [tags.freshVege], []),
    getMenuItem(tags.main, [tags.main], [
      getMenuItem(tags.beef, [tags.beef], []),
      getMenuItem(tags.calamari, [tags.calamari], []),
      getMenuItem(tags.fish, [tags.fish], []),
      getMenuItem(tags.prawn, [tags.prawn], []),
      getMenuItem(tags.scallop, [tags.scallop], []),
      getMenuItem(tags.chicken, [tags.chicken], []),
      getMenuItem(tags.lamb, [tags.lamb], []),
      getMenuItem(tags.duck, [tags.duck], []),
      getMenuItem(tags.pork, [tags.pork], []),
      getMenuItem(tags.seafood, [tags.seafood], [])
    ]),
    getMenuItem(tags.malay, [tags.malay], [
      getMenuItem(tags.curry, [tags.curry], []),
      getMenuItem(tags.thaiChilli, [tags.thaiChilli], []),
      getMenuItem(tags.malaySatay, [tags.malaySatay], []),
      getMenuItem(tags.laksa, [tags.laksa], []),
      getMenuItem(tags.malayChilli, [tags.malayChilli], []),
      getMenuItem(tags.padThai, [tags.padThai], []),
    ]),
    getMenuItem(tags.viet, [tags.viet], []),
    getMenuItem(tags.omelette, [tags.omelette], []),
    getMenuItem(tags.noodles, [tags.noodles], []),
    getMenuItem(tags.chowMein, [tags.chowMein], []),
    getMenuItem(tags.rice, [tags.rice], []),
    getMenuItem(tags.conventional, [tags.conventional], []),
    getMenuItem(tags.packs, [tags.packs], []),
    getMenuItem(tags.drinks, [tags.drinks], []),
  ]
}